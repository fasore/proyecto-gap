﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoeServices
{
    public class Store
    {
        private int id;
        private string name;
        private string address;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

    }
}