﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace ShoeServices
{
    /// <summary>
    /// Summary description for Services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Services : System.Web.Services.WebService
    {


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Stores()
        {
            ServiceClass service = new ServiceClass();
            List<Store> storeList = new List<Store>();
            string connString = ConfigurationManager.ConnectionStrings["ConexionDB"].ToString(); ;
            string query = "select * from Store";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            //query database and return the result
            da.Fill(dataTable);
            foreach(DataRow storeRow in dataTable.Rows)
            {
                Store storeData = new Store();
                storeData.ID = Convert.ToInt32(storeRow["Id"]);
                storeData.Name = Convert.ToString(storeRow["Name"]);
                storeData.Address = Convert.ToString(storeRow["Address"]);
                storeList.Add(storeData);
            }
            conn.Close();
            da.Dispose();
            service.Stores = storeList;
            service.Success = true;
            
            return new JavaScriptSerializer().Serialize(service);
        }

        //Gets all articles
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Articles()
        {
            ArticleService service = new ArticleService();
            List<Article> articleList = new List<Article>();
            string connString = ConfigurationManager.ConnectionStrings["ConexionDB"].ToString(); ;
            string query = "select * from Article";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            //query database and return the result
            da.Fill(dataTable);
            foreach (DataRow articleRow in dataTable.Rows)
            {
                Article articleData = new Article();
                articleData.ID = Convert.ToInt32(articleRow["Id"]);
                articleData.Name = Convert.ToString(articleRow["Name"]);
                articleData.Price = Convert.ToDecimal(articleRow["Price"]);
                articleData.TotalInShelf = Convert.ToInt32(articleRow["TotalInShelf"]);
                articleData.TotalInVault = Convert.ToInt32(articleRow["TotalInVault"]);
                articleData.StoreID = Convert.ToInt32(articleRow["StoreId"]);
                articleList.Add(articleData);
            }
            conn.Close();
            da.Dispose();
            service.Articles = articleList;
            service.Success = true;
            return new JavaScriptSerializer().Serialize(service);
        }

        //Gets all articles from an specific Store
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ArticlesFromStore(int StoreId)
        {
            ArticleService service = new ArticleService();
            List<Article> articleList = new List<Article>();
            string connString = ConfigurationManager.ConnectionStrings["ConexionDB"].ToString(); ;
            string query = String.Format("select * from Article where StoreId = {0}", StoreId.ToString());

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();

            // create data adapter
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            //query database and return the result
            da.Fill(dataTable);
            foreach (DataRow articleRow in dataTable.Rows)
            {
                Article articleData = new Article();
                articleData.ID = Convert.ToInt32(articleRow["Id"]);
                articleData.Name = Convert.ToString(articleRow["Name"]);
                articleData.Price = Convert.ToDecimal(articleRow["Price"]);
                articleData.TotalInShelf = Convert.ToInt32(articleRow["TotalInShelf"]);
                articleData.TotalInVault = Convert.ToInt32(articleRow["TotalInVault"]);
                articleData.StoreID = Convert.ToInt32(articleRow["StoreId"]);
                articleList.Add(articleData);
            }
            conn.Close();
            da.Dispose();
            service.Articles = articleList;
            service.Success = true;
            return new JavaScriptSerializer().Serialize(service);
        }
    }

}
