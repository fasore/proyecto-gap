﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoeServices
{
    public class ServiceClass
    {
        private bool success;
        private List<Store> stores;

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        public List<Store> Stores
        {
            get { return stores; }
            set { stores = value; }
        }

    }
}