﻿using SuperZapatosApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace SuperZapatosApp.Controllers
{
    public class ServicesController : Controller
    {
        [System.Web.Http.HttpGet]
        public JsonResult Stores()
        {
            ArticleContext db = new ArticleContext();
            var result = new JsonResult();
            result.Data = new
            {
                success = true,
                stores = db.Stores.ToList()
            };
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

    }
}
