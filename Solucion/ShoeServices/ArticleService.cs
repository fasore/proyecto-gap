﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoeServices
{
    public class ArticleService
    {
        private bool success;
        private List<Article> articles;

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        public List<Article> Articles
        {
            get { return articles; }
            set { articles = value; }
        }
    }
}