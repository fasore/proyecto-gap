﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoeServices
{
    public class Article
    {
        private int id;
        private string name;
        private string description;
        private decimal price;
        private int totalInShelf;
        private int totalInVault;
        private int storeId;


        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }

        public int TotalInShelf
        {
            get { return totalInShelf; }
            set { totalInShelf = value; }
        }

        public int TotalInVault
        {
            get { return totalInVault; }
            set { totalInVault = value; }
        }

        public int StoreID
        {
            get { return storeId; }
            set { storeId = value; }
        }
    }
}